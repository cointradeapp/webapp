import moment from 'moment'

const urlParser = document.createElement('a')
const digitsRE = /(\d{3})(?=\d)/g

export function domain (url) {
  urlParser.href = url
  return urlParser.hostname
}

export function money (amount, currency) {
    var value = amount / 100;
    value = parseFloat(value)
    if (!isFinite(value) || (!value && value !== 0)) return ''
    currency = currency != null ? currency : '$'
    var stringified = Math.abs(value).toFixed(2)
    var _int = stringified.slice(0, -3)
    var i = _int.length % 3
    var head = i > 0
      ? (_int.slice(0, i) + (_int.length > 3 ? ',' : ''))
      : ''
    var _float = stringified.slice(-3)
    var sign = value < 0 ? '-' : ''
    var total = currency + sign + head +
      _int.slice(i).replace(digitsRE, '$1,') +
      _float
      return total.toString().split(".")[0];
}

export function price (value) {
    return "$" + value / 100
}

export function fromNow(date) {
  return moment(date).fromNow();
}

export function day(date) {
  return moment(date).format("MMM Do YY");
}

export function cleanDate(date) {
  var str = date.split("in")[1]

  return str.replace("a ", "1 ");
}
