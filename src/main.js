import Vue from 'vue'
import Router from 'vue-router'
import Resource from 'vue-resource'
import {domain, money, fromNow, day, cleanDate, price} from './filters'
import App from './components/App.vue'
import Dashboard from './components/Dashboard.vue'
import InvestmentHistory from './components/InvestmentHistory.vue'
import InvestmentFinder from './components/InvestmentFinder.vue'
import Login from './components/Login.vue'
import InvestmentDetails from './components/InvestmentDetails.vue'
import Feedback from './components/Feedback.vue'
import auth from './services/auth'
import moment from 'moment'

// install router
Vue.use(Router)
Vue.use(Resource)

// Vue resource config
Vue.http.options.root = "http://localhost:8000"

// register filters globally
Vue.filter('domain', domain)
Vue.filter('money', money)
Vue.filter('fromNow', fromNow)
Vue.filter('day', day)
Vue.filter('cleanDate', cleanDate)
Vue.filter('price', price)

Vue.filter('date', function(date){
  return date * 100;
});

// Check the user's auth status when the app starts
auth.checkAuth()

// routing
export var router = new Router()

router.map({
  '/:username/dashboard': {
    name: 'dashboard',
    component: Dashboard
  },
  '/:username/feedback': {
    name: 'feedback',
    component: Feedback
  },
  '/:username/investment-finder': {
    name: 'investment-finder',
    component: InvestmentFinder
  },
  '/:username/investment-history': {
    name: 'investment-history',
    component: InvestmentHistory
  },
  '/:username/investment-details/:investmentId':{
    name: 'investment-details',
    component: InvestmentDetails
  },
  '/auth/login': {
    name: 'login',
    component: Login
  }
})

router.beforeEach(function () {
  var bodyWrapper = document.getElementById("bodyWrapper");
  bodyWrapper.scrollTop = 0;
})

router.redirect({
  '*': '/:username/dashboard'
})

router.start(App, '#app')
