export default {
  getInvestments(ctx, limit) {
    return ctx.$http.get('http://localhost:8000/api/investments?limit='+limit, function (data, status, request) {
          return data;
      }).error(function (data, status, request) {
          return data;
      })
  },

  getInvestment(ctx, id) {
    return ctx.$http.get('http://localhost:8000/api/investments/'+id, function (data, status, request) {
          return data;
      }).error(function (data, status, request) {
          return data;
      })
  },

  getUserInvestments(ctx, id, limit) {
    return ctx.$http.get('http://localhost:8000/api/users/'+id+'/investments?limit='+limit, function (data, status, request) {
          return data;
      }).error(function (data, status, request) {
          return data;
      })
  },    

  buy(ctx, creds) {
    return ctx.$http.post('http://localhost:8000/api/investments/buy', creds);
  }
}
